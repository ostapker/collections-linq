﻿
namespace Collections_Linq
{
    public static class Constants
    {
        public const string BaseUrl = "https://bsa20.azurewebsites.net";
        public const int MaxNameLength = 45;
        public const int MinAge = 10;
        public const int MinDescriptionLength = 20;
        public const int MaxTasksCount = 3;
        public const int Indent = 16;
    }
}
