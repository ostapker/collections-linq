﻿
namespace Collections_Linq.Enums
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
