﻿using Collections_Linq.Services;
using System;

namespace Collections_Linq
{
    public class Facade
    {
        private readonly ProgramService programService;

        public Facade()
        {
            programService = new ProgramService();
        }

        public void Start()
        {
            while (true)
            {
                Console.WriteLine("Choose command:");
                Console.WriteLine("1 - print tasks amount in specific user's projects");
                Console.WriteLine($"2 - print task list performed by user, where task name < {Constants.MaxNameLength} characters");
                Console.WriteLine($"3 - print task list finished in currect({DateTime.Now.Year})");
                Console.WriteLine($"4 - print users older {Constants.MinAge} years, grouped by teams");
                Console.WriteLine("5 - print users sorted by first_name with tasks sorted by name length descending");
                Console.WriteLine("6 - print user's info");
                Console.WriteLine("7 - print projects info");
                Console.WriteLine("0 - exit");
                var commandString = Console.ReadLine();
                Console.WriteLine();
                try
                {
                    int command = int.Parse(commandString);

                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            PrintTaskCountInProjectsByUser();
                            break;
                        case 2:
                            PrintTasksPerformedByUser();
                            break;
                        case 3:
                            PrintFinishedTasksByUser();
                            break;
                        case 4:
                            PrintUsersGroupedByTeam();
                            break;
                        case 5:
                            PrintTasksGroupedByUser();
                            break;
                        case 6:
                            PrintUserInfo();
                            break;
                        case 7:
                            PrintProjectsInfo();
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine($"Incorrect command");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Console.WriteLine();
            }
        }

        public int ReadId()
        {
            var idString = Console.ReadLine();
            var id = -1;
            try
            {
                id = int.Parse(idString);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            return id;
        }

        public void PrintTaskCountInProjectsByUser()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadId();

            try
            {
                foreach (var kvp in programService.GetTaskCountInProjectsByUser(userId))
                {
                    Console.WriteLine($"Project:\n{kvp.Key.ToString($"{"",-Constants.Indent}")}\n{"TaskCount:",-Constants.Indent}{kvp.Value}\n");
                }
            }
            catch(Exception e)
            {
                throw e;
            }

        }

        public void PrintTasksPerformedByUser()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadId();

            try
            {
                foreach (var task in programService.GetTasksPerformedByUser(userId))
                {
                    Console.WriteLine(task);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void PrintFinishedTasksByUser()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadId();

            try
            {
                foreach (var task in programService.GetFinishedTasksByUser(userId))
                {
                    Console.WriteLine(task);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void PrintUsersGroupedByTeam()
        {
            try
            {
                foreach (var item in programService.GetUsersGroupedByTeam())
                {
                    Console.WriteLine(item);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void PrintTasksGroupedByUser()
        {
            try
            {
                foreach (var item in programService.GetTasksGroupedByUser())
                {
                    Console.Write(item);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void PrintUserInfo()
        {
            Console.WriteLine("Enter user id:");
            var userId = ReadId();
            try
            {
                var userInfo = programService.GetUserInfo(userId);

                Console.Write(userInfo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void PrintProjectsInfo()
        {
            try
            {
                foreach (var projectInfo in programService.GetProjectsInfo())
                {
                    Console.WriteLine(projectInfo);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}