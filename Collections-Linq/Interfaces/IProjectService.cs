﻿using Collections_Linq.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Collections_Linq.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectModel>> GetProjectsAsync();

        Task<ProjectModel> GetProjectByIdAsync(int id);
    }
}
