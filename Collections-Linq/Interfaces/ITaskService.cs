﻿using Collections_Linq.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Collections_Linq.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskModel>> GetTasksAsync();

        Task<TaskModel> GetTaskByIdAsync(int id);
    }
}
