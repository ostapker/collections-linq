﻿using Collections_Linq.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Collections_Linq.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamModel>> GetTeamsAsync();

        Task<TeamModel> GetTeamByIdAsync(int id);
    }
}
