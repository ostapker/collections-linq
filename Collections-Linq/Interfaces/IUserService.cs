﻿using Collections_Linq.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Collections_Linq.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserModel>> GetUsersAsync();

        Task<UserModel> GetUserByIdAsync(int id);
    }
}
