﻿using Newtonsoft.Json;

namespace Collections_Linq.Models
{
    public abstract class BaseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
