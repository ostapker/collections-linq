﻿
namespace Collections_Linq.Models.ExtendedModels
{
    public class ProjectDetailedInfoModel
    {
#nullable enable
        public ProjectModel? Project { get; set; }
        public TaskModel? LongestTask { get; set; }
        public TaskModel? ShortestTask { get; set; }
        public int TeamMembersCount { get; set; }

        public override string ToString()
        {
            return $"{Project}\nLongest task by description:\n{LongestTask?.ToString($"{"",-Constants.Indent}")}\n"
                   + $"Shortest task by name:\n{ShortestTask?.ToString($"{"",-Constants.Indent}")}\n"
                   + $"Team Members Count:\t{TeamMembersCount}\n";
        }
    }
}
