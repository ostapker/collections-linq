﻿
namespace Collections_Linq.Models.ExtendedModels
{
    public class TaskFinishedModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n";
        }
    }
}
