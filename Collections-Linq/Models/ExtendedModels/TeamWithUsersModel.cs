﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections_Linq.Models.ExtendedModels
{
    public class TeamWithUsersModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UserModel> Users { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n"
                + $"users:\n{Users.Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }
    }
}
