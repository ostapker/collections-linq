﻿
namespace Collections_Linq.Models.ExtendedModels
{
    public class UserDetailedInfoModel
    {
#nullable enable
        public UserModel? User { get; set; }
        public ProjectModel? LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskModel? LongestTask { get; set; }

        public override string ToString()
        {
            return $"{User}\nLast Project:\n{LastProject?.ToString($"{"",-Constants.Indent}")}\n"
                   + $"Last Project Tasks Count:\t{LastProjectTasksCount}\n"
                   + $"Unfinished Tasks Count:\t\t{UnfinishedTasksCount}\n"
                   + $"Longest task by duration:\n{LongestTask?.ToString($"{"",-Constants.Indent}")}\n";
        }
    }
}
