﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections_Linq.Models.ExtendedModels
{
    public class UserWithTasksModel
    {
        public UserModel User { get; set; }
        public ICollection<TaskModel> Tasks { get; set; }

        public override string ToString()
        {
            return $"{User}"
                + $"tasks:\n{Tasks.Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }
    }
}
