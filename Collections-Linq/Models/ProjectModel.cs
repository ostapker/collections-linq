﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Collections_Linq.Models
{
    public class ProjectModel : BaseModel
    {

#nullable enable
        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        public string? Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        public ICollection<TaskModel>? Tasks { get; set; }

        public UserModel? Author { get; set; }

        public TeamModel? Team { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n"
                +$"{"description:",-Constants.Indent}{Regex.Replace(Description, @"\n", $"\n{"",-Constants.Indent}")}\n"
                +$"{"createdAt:",-Constants.Indent}{CreatedAt}\n{"deadline:",-Constants.Indent}{Deadline}\n"
                +$"{"authorId:",-Constants.Indent}{AuthorId}\n"
                +$"author:\n{Author?.ToString($"{"",-Constants.Indent}")}\n"
                +$"{"teamId:",-Constants.Indent}{TeamId}\n"
                +$"team:\n{Team?.ToString($"{"",-Constants.Indent}")}\n"
                +$"tasks:\n{Tasks.Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
