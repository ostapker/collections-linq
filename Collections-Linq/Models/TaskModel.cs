﻿using Collections_Linq.Enums;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace Collections_Linq.Models
{
    public class TaskModel : BaseModel
    {
#nullable enable
        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        public string? Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public TaskState State { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonProperty("performerId")]
        public int PerformerId { get; set; }

        public UserModel? Performer { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n"
                +$"{"description:",-Constants.Indent}{Regex.Replace(Description, @"\n", $"\n{"",-Constants.Indent}")}\n"
                +$"{"createdAt:",-Constants.Indent}{CreatedAt}\n{"finishedAt:",-Constants.Indent}{FinishedAt}\n"
                +$"{"state:",-Constants.Indent}{State}\n"
                +$"{"projectId:",-Constants.Indent}{ProjectId}\n{"performerId:",-Constants.Indent}{PerformerId}\n"
                +$"performer:\n{Performer?.ToString($"{"",-Constants.Indent}")}\n";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
