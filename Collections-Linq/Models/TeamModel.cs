﻿using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace Collections_Linq.Models
{
    public class TeamModel : BaseModel
    {
#nullable enable
        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n{"createdAt:",-Constants.Indent}{CreatedAt}";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
