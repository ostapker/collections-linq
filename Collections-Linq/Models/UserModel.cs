﻿using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace Collections_Linq.Models
{
    public  class UserModel : BaseModel
    {
#nullable enable
        [JsonProperty("firstName")]
        public string? FirstName { get; set; }

        [JsonProperty("lastName")]
        public string? LastName { get; set; }

        [JsonProperty("email")]
        public string? Email { get; set; }

        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }

        [JsonProperty("registeredAt")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"firstName:",-Constants.Indent}{FirstName}\n"
                +$"{"lastName:",-Constants.Indent}{LastName}\n{"email:",-Constants.Indent}{Email}\n"
                +$"{"birthday:",-Constants.Indent}{Birthday}\n{"registeredAt:",-Constants.Indent}{RegisteredAt}\n"
                +$"{"teamId:",-Constants.Indent}{TeamId}\n";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
