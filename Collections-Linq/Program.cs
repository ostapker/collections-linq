﻿
namespace Collections_Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            var facade = new Facade();
            facade.Start();
        }
    }
}
