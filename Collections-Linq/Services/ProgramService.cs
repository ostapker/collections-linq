﻿using Collections_Linq.Enums;
using Collections_Linq.Interfaces;
using Collections_Linq.Models;
using Collections_Linq.Models.ExtendedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Collections_Linq.Services
{
    public class ProgramService
    {
        private readonly IProjectService projectService;
        private readonly ITaskService taskService;
        private readonly ITeamService teamService;
        private readonly IUserService userService;

        private ICollection<ProjectModel> Projects;
        private ICollection<TaskModel> Tasks;
        private ICollection<UserModel> Users;
        private ICollection<TeamModel> Teams;

        public ProgramService()
        {
            var httpClient = new HttpClient();
            projectService = new ProjectService(httpClient);
            taskService = new TaskService(httpClient);
            teamService = new TeamService(httpClient);
            userService = new UserService(httpClient);

            FetchData();
        }

        private void FetchData()
        {
            Users = userService.GetUsersAsync().Result.ToList();

            Tasks = taskService.GetTasksAsync().Result
                                    .Join(Users,
                                    task => task.PerformerId,
                                    user => user.Id,
                                    (task, user) =>
                                    {
                                        task.Performer = user;
                                        return task;
                                    }).ToList();

            Teams = teamService.GetTeamsAsync().Result.ToList();

            Projects = projectService.GetProjectsAsync().Result
                        .GroupJoin(Tasks,
                                project => project.Id,
                                task => task.ProjectId,
                                (project, tasks) =>
                                {
                                    project.Tasks = tasks.ToList();
                                    return project;
                                })
                        .Join(Users,
                                project => project.AuthorId,
                                user => user.Id,
                                (project, user) =>
                                {
                                    project.Author = user;
                                    return project;
                                })
                        .Join(Teams,
                                project => project.TeamId,
                                team => team.Id,
                                (project, team) => {
                                    project.Team = team;
                                    return project;
                                })
                        .ToList();
        }

        public Dictionary<ProjectModel, int> GetTaskCountInProjectsByUser(int userId)
        {
            var projects = Projects.Where(project => project.AuthorId == userId)
                                    .GroupBy(project => project)
                                    .ToDictionary(g => g.Key, g => g.Key.Tasks.Count);
            
            if (projects.Count == 0)
                throw new Exception("Not found");
            
            return projects;
        }

        public IEnumerable<TaskModel> GetTasksPerformedByUser(int userId)
        {
            var tasks = Tasks.Where(task => task.PerformerId == userId && task.Name.Length < Constants.MaxNameLength)
                                .ToList();
            
            if(tasks.Count == 0)
                throw new Exception("Not found");

            return tasks;
        }

        public IEnumerable<TaskFinishedModel> GetFinishedTasksByUser(int userId)
        {
            var tasks = Tasks.Where(task => task.PerformerId == userId && task.State == TaskState.Finished && task.FinishedAt.Year == DateTime.Now.Year)
                            .Select(task => new TaskFinishedModel{ Id = task.Id, Name = task.Name })
                            .ToList();
            
            if (tasks.Count == 0)
                throw new Exception("Not found");

            return tasks;
        }

        public IEnumerable<TeamWithUsersModel> GetUsersGroupedByTeam()
        {
            var teams = Teams.GroupJoin(Users.Where(user => user.TeamId != null && DateTime.Now.Year - user.Birthday.Year > Constants.MinAge)
                                              .OrderByDescending(user => user.RegisteredAt),
                                    team => team.Id,
                                    user => user.TeamId,
                                    (team, users) => new TeamWithUsersModel
                                    {
                                        Id = team.Id,
                                        Name = team.Name,
                                        Users = users.ToList()
                                    })
                                .Where(item => item.Users.Count > 0)
                                .ToList();
            
            if (teams.Count == 0)
                throw new Exception("Not found");

            return teams;
        }

        public IEnumerable<UserWithTasksModel> GetTasksGroupedByUser()
        {
            var users = Tasks.OrderByDescending(task => task.Name.Length)
                               .GroupBy(task => task.Performer)
                               .OrderBy(grp => grp.Key.FirstName)
                               .Select(grp => new UserWithTasksModel
                               {
                                   User = grp.Key,
                                   Tasks = grp.ToList()
                               })
                               .ToList();

            if (users.Count == 0)
                throw new Exception("Not found");

            return users;
        }

        public UserDetailedInfoModel GetUserInfo(int userId)
        {
            var user = Users.Where(user => user.Id == userId)
                            .Select(user => {
                                var lastProject = Projects.Where(project => project.AuthorId == user.Id)
                                                           .OrderByDescending(project => project.CreatedAt)
                                                           .FirstOrDefault();
                                var tasks = Tasks.Where(task => task.PerformerId == user.Id);
                                return new UserDetailedInfoModel
                                {
                                    User = user,
                                    LastProject = lastProject,
                                    LastProjectTasksCount = lastProject != null ? lastProject.Tasks.Count : default,
                                    UnfinishedTasksCount = tasks.Count(task => task.State != TaskState.Finished),
                                    LongestTask = tasks.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault()
                                };
                            })
                            .FirstOrDefault();

            if (user == null)
                throw new Exception("Not found");

            return user;
        }

        public IEnumerable<ProjectDetailedInfoModel> GetProjectsInfo()
        {
            var projects = Projects.Select(project => new ProjectDetailedInfoModel
                                        {
                                            Project = project,
                                            LongestTask = project.Tasks.OrderByDescending(task => task.Description).FirstOrDefault(),
                                            ShortestTask = project.Tasks.OrderBy(task => task.Name).FirstOrDefault(),
                                            TeamMembersCount = (project.Description.Length > Constants.MinDescriptionLength || project.Tasks.Count < Constants.MaxTasksCount) ?
                                                                Users.Where(user => user.TeamId == project.Team.Id).Count() : default
                                        })
                                    .ToList();

            if (projects.Count == 0)
                throw new Exception("Not found");

            return projects;
        }
    }
}