﻿using Collections_Linq.Interfaces;
using Collections_Linq.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_Linq.Services
{
    public class ProjectService : IProjectService
    {
        private HttpClient _client;

        public ProjectService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<ProjectModel>> GetProjectsAsync()
        {
            var projects = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectModel>>(projects);
        }

        public async Task<ProjectModel> GetProjectByIdAsync(int id)
        {
            var project = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Projects/{id}");
            return JsonConvert.DeserializeObject<ProjectModel>(project);
        }
    }
}
