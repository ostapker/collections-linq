﻿using Collections_Linq.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_Linq.Services
{
    public class TaskService : ITaskService
    {
        private HttpClient _client;

        public TaskService(HttpClient client)
        {
            _client = client;
        }

        public async Task<Models.TaskModel> GetTaskByIdAsync(int id)
        {
            var task = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Tasks/{id}");
            return JsonConvert.DeserializeObject<Models.TaskModel>(task);
        }

        public async Task<IEnumerable<Models.TaskModel>> GetTasksAsync()
        {
            var tasks = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Tasks");
            return JsonConvert.DeserializeObject<IEnumerable<Models.TaskModel>>(tasks);
        }
    }
}
