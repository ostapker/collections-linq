﻿using Collections_Linq.Interfaces;
using Collections_Linq.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_Linq.Services
{
    public class TeamService : ITeamService
    {
        private HttpClient _client;

        public TeamService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<TeamModel>> GetTeamsAsync()
        {
            var teams = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Teams");
            return JsonConvert.DeserializeObject<IEnumerable<TeamModel>>(teams);
        }

        public async Task<TeamModel> GetTeamByIdAsync(int id)
        {
            var team = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Teams/{id}");
            return JsonConvert.DeserializeObject<TeamModel>(team);
        }
    }
}
