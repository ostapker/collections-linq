﻿using Collections_Linq.Interfaces;
using Collections_Linq.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_Linq.Services
{
    public class UserService : IUserService
    {
        private HttpClient _client;

        public UserService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<UserModel>> GetUsersAsync()
        {
            var users = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Users");
            return JsonConvert.DeserializeObject<IEnumerable<UserModel>>(users);
        }

        public async Task<UserModel> GetUserByIdAsync(int id)
        {
            var user = await _client.GetStringAsync($"{Constants.BaseUrl}/api/Users/{id}");
            return JsonConvert.DeserializeObject<UserModel>(user);
        }
    }
}
